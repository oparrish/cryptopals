import operator

def xor(a, b):
    if len(a) != len(b):
        raise Exception('Byte arrays are not the same length')

    result = bytearray(len(a))

    for i in range(0, len(a)):
        result[i] = a[i] ^ b[i]

    return bytes(result)