import challenge1
import challenge2
import challenge3
import challenge4
import unittest
import string
import random


class set1(unittest.TestCase):
    def test_challenge1(self):
        hex = bytes.fromhex(
            '49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d')
        base64result = challenge1.hextobase64(hex)
        expected = b'SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t'

        self.assertEqual(
            expected,
            base64result
        )

    def test_challege2(self):
        a = bytes.fromhex('1c0111001f010100061a024b53535009181c')
        b = bytes.fromhex('686974207468652062756c6c277320657965')
        xord = challenge2.xor(a, b)
        expected = bytes.fromhex('746865206b696420646f6e277420706c6179')
        self.assertEqual(
            expected,
            xord
        )

    def test_challege2_different_length(self):
        a = bytes.fromhex('1c0111001f010100061a024b53535009181c')
        b = bytes.fromhex('adfd')
        with self.assertRaises(Exception):
            xord = challenge2.xor(a, b)

    def test_challenge3_generated(self):
        key = random.choice(string.ascii_letters)
        plain = "i think i know it all but can i be sure".encode(
            'ascii', 'backslashreplace').hex()
        ciphertext = challenge3.decrypt(
            plain, key).decode().encode('ascii').hex()
        broken_key = challenge3.break_single_xor(ciphertext)
        self.assertEqual(key, broken_key)

    def test_challenge3(self):
        challenge3_ciphertext = '1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736'
        key = challenge3.break_single_xor(challenge3_ciphertext)
        self.assertEqual('X', key)

    def test_challenge4(self):
        result = challenge4.break_encrypted_file()
        self.assertEqual(
            (1.7111, 'Now that the party is jumping', '5'), result)
