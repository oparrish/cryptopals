import challenge3
from challenge3 import KeyNotFoundException


def break_encrypted(ciphertext):
    try:
        key = challenge3.break_single_xor(ciphertext)
        plain = challenge3.decrypt(ciphertext, key).decode('ascii')
        score = round(challenge3.score_sentence(plain), 4)

        return score, key, plain
    except (UnicodeDecodeError, KeyNotFoundException):
        return float('inf'), '', ''


def break_encrypted_file():
    best_score = float('inf')

    with open('set1/4.txt') as encrypted:
        for line in encrypted.readlines():
            line = line.strip()
            score, key, plain = break_encrypted(line)

            if score < best_score:
                best_score = score
                likely_key = key
                likely_plain = plain.strip()

    return best_score, likely_plain, likely_key
