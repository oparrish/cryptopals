import string
import challenge2

# https://www3.nd.edu/~busiforc/handouts/cryptography/Letter%20Frequencies.html
LETTER_FREQUENCIES = {
    'A': 0.0651738,
    'B': 0.0124248,
    'C': 0.0217339,
    'D': 0.0349835,
    'E': 0.1041442,
    'F': 0.0197881,
    'G': 0.0158610,
    'H': 0.0492888,
    'I': 0.0558094,
    'J': 0.0009033,
    'K': 0.0050529,
    'L': 0.0331490,
    'M': 0.0202124,
    'N': 0.0564513,
    'O': 0.0596302,
    'P': 0.0137645,
    'Q': 0.0008606,
    'R': 0.0497563,
    'S': 0.0515760,
    'T': 0.0729357,
    'U': 0.0225134,
    'V': 0.0082903,
    'W': 0.0171272,
    'X': 0.0013692,
    'Y': 0.0145984,
    'Z': 0.0007836,
    ' ': 0.1918182
}


def count_letters(sentence):
    letter_count_map = {}

    # Get raw count per letter
    for letter in sentence:
        letter = letter.upper()

        if letter not in LETTER_FREQUENCIES.keys():
            continue

        letter_count = letter_count_map.get(letter, 0)
        letter_count += 1

        letter_count_map[letter] = letter_count

    # convert raw count to relative frequency
    for letter, letter_count in letter_count_map.items():
        letter_count_percent = round((letter_count / len(sentence)), 4)

        letter_count_map[letter] = letter_count_percent

    full_letter_counts = {}

    # create a full result that includes all letters
    for letter in LETTER_FREQUENCIES:
        full_letter_counts[letter] = letter_count_map.get(letter, 0)

    return full_letter_counts


def calculate_chi_squared(observed_frequencies, expected_frequencies):
    chi_squared = [((observed - expected)**2) / expected for observed,
                   expected in zip(observed_frequencies, expected_frequencies)]

    return sum(chi_squared)

# Score the sentence by calculating the chi squared of the letter frequencies


def score_sentence(sentence):
    # first check ratio of letters
    # if over arbitrary threshold, reject
    if sum([1 if letter.isalpha() or letter == ' ' else 0 for letter in sentence])/len(sentence) < 0.8:
        return float('inf')

    observed_frequencies = [value for _,
                            value in count_letters(sentence).items()]
    expected_frequencies = [value for _, value in LETTER_FREQUENCIES.items()]

    chi_squared = calculate_chi_squared(
        observed_frequencies, expected_frequencies)

    return chi_squared


def decrypt(ciphertext, key):
    cipher_bytes = bytes.fromhex(ciphertext)
    plain_bytes = bytearray(len(cipher_bytes))

    key = bytes(key * len(cipher_bytes), 'ascii')

    plain_bytes = challenge2.xor(cipher_bytes, key)

    return plain_bytes


def break_single_xor(ciphertext):
    best_score = float('inf')
    likely_key = ''

    # most likley key is the key that generates plaintext
    # with the lowest score
    for key in string.printable:
        try:
            plain = decrypt(ciphertext, key).decode(
                'ascii', 'backslashreplace')
        # If we can't even decode the plaintext, this is not the key
        except UnicodeEncodeError as e:
            continue

        score = score_sentence(plain)

        if score < best_score:
            best_score = score
            likely_key = key

    if likely_key == '':
        raise KeyNotFoundException

    return likely_key


class KeyNotFoundException(Exception):
    pass
