import base64


def hextobase64(hex_bytes):
    result = base64.standard_b64encode(hex_bytes)
    return result
